swagger: '2.0'

info:
  version: NA
  title: Logistic Regression Classifier - Movesense-ML-API
  description:
    This file defines interface for the Logistic regression based Classifier API.
  x-api-state: experimental
  x-api-required: false
  x-api-type: public

paths:
  /Ml/Classifier/LogisticRegression:
    post:
      summary: Create a multinomial logistic regression classifier.
      parameters:
      - in: body
        name: LogisticRegressionParameters
        required: true
        schema:
          $ref: "#/definitions/LogisticRegressionParameters"
      responses:
        201:
          description: New classifier added. Classifier handle is provided.
          schema:
            $ref: './types.yaml#/definitions/EndpointHandle'
        400:
          description: Bad request. Operation failed. Maximum active classifier count exceeded.
        x-std-errors:
          description: See common error codes http://developer.suunto.com/api/std-errors#unsubscribe

  /Ml/Classifier/LogisticRegression/{Id}:
    get:
      description: Class id with the confidence value in the range 0 ... 1. 0 means invalid result.
      parameters:
      - in: path
        name: Id
        required: true
        type: integer
        format: int32
      responses:
        200:
          description: New classifier added. The new class id with confidence is returned.
          schema:
            $ref: "./types.yaml#/definitions/Scalar"
        400:
          description: Bad request. Operation failed. Maximum active classifier count exceeded.
        404:
          description: Unknown logistic regression id.
        x-std-errors:
          description: See common error codes http://developer.suunto.com/api/std-errors#unsubscribe
    delete:
      summary: Delete the logistic regression classifier.
      parameters:
      - in: path
        name: Id
        required: true
        type: integer
        format: int32
      responses:
        200:
          description: The classfier was successfully deleted.
        404:
          description: Unknown logistic regression id.
        x-std-errors:
          description: See common error codes http://developer.suunto.com/api/std-errors#unsubscribe

definitions:
  LogisticRegressionParameters:
    required:
      - ClassId
      - Bias
      - Weights
    properties:
      ClassId:
        type: integer
        format: int32
      Bias:
        type: number
        format: float
      Weights:
        type: array
        items:
          $ref: "#/definitions/LogisticRegressionWeight"
      ClassIdOther:
        type: integer
        format: int32

  LogisticRegressionWeight:
    required:
      - Handle
      - Weight
    properties:
      Handle:
        $ref: "./types.yaml#/definitions/EndpointHandle"
      Weight:
        type: number
        format: float
