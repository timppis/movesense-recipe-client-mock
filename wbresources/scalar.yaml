swagger: '2.0'

info:
  version: NA
  title: Scalar - Movesense-API
  description: |
    Service for extracting/calculating scalar value for the given input.
  x-api-state: experimental
  x-api-required: false
  x-api-type: public

# Paths
paths:
  /Meas/Scalar:
    post:
      summary: Create a new scalar endpoint with a unique scalar id.
      parameters:
      - in: body
        name: ScalarParameters
        required: true
        schema:
          $ref: '#/definitions/ScalarParameters'
      responses:
        201:
          description: The request was succesful.
          schema:
            $ref: './types.yaml#/definitions/EndpointHandle'
        x-std-errors:
          description: See common error codes http://developer.suunto.com/api/std-errors#unsubscribe
  /Meas/Scalar/{ScalarId}:
    get:
      description: |
        Get the scalar properties. These will be used to compress buffers.
      parameters:
      - in: path
        name: ScalarId
        required: true
        type: integer
        format: int32
      responses:
        200:
          description: Success
          schema:
            $ref: '#/definitions/ScalarProperties'
        x-std-errors:
          description: See common error codes http://developer.suunto.com/api/std-errors#unsubscribe
    delete:
      description: |
        Delete scalar endpoint.
      parameters:
      - in: path
        name: ScalarId
        required: true
        type: integer
        format: int32
      responses:
        200:
          description: Operation completed successfully
        x-std-errors:
          description: See common error codes http://developer.suunto.com/api/std-errors#unsubscribe
  /Meas/Scalar/{ScalarId}/Subscription:
    post:
      summary: Subscribe to a scalar value.
      parameters:
      - in: path
        name: ScalarId
        required: true
        type: integer
        format: int32
      responses:
        201:
          description: The request was succesful.
        x-std-errors:
          description: See common error codes http://developer.suunto.com/api/std-errors#unsubscribe
        x-notification:
          schema:
            $ref: './types.yaml#/definitions/ScalarArray'
    delete:
      description: |
        Unsubscribe from the scalar values.
      parameters:
      - in: path
        name: ScalarId
        required: true
        type: integer
        format: int32
      responses:
        200:
          description: Operation completed successfully
        x-std-errors:
          description: See common error codes http://developer.suunto.com/api/std-errors#unsubscribe

definitions:

  ScalarParameters:
    required:
      - MeasId
      - ComponentId
      - SampleRate
    properties:
      MeasId:
        type: integer
        format: uint8
      ComponentId:
        type: integer
        format: uint8
      SampleRate:
        type: integer
        format: int32
      EuclideanDifferenceReference:
        $ref: "#/definitions/EuclideanDifferenceReference"

  ScalarMeasId:
    type: integer
    format: uint8
    enum:
      - name: Acc
        value: 0
      - name: Gyro
        value: 1
      - name: Magn
        value: 2

  ScalarComponentId:
    type: integer
    format: uint8
    enum:
      - name: X
        value: 0
      - name: Y
        value: 1
      - name: Z
        value: 2
      - name: Norm
        value: 3
      - name: Sum
        value: 4
      - name: XYDifference
        value: 5
      - name: YZDifference
        value: 6
      - name: ZXDifference
        value: 7
      - name: RelativeAmplitude
        value: 8
      - name: EuclideanDistance
        value: 9

  EuclideanDifferenceReference:
    required:
      - Values
    properties:
      Values:
        description: |
          Array item order:
           3D vector: x,y,x
           IMU6: acc_x, acc_y, acc_z, gyro_x, gyro_y, gyro_z
           IMU9: acc_x, acc_y, acc_z, gyro_x, gyro_y, gyro_z, magn_x, magn_y, magn_z
        type: array
        items:
          type: number
          format: float

  ScalarProperties:
    required:
      - Format
    properties:
      Format:
        $ref: '#/definitions/ScalarFormat'
      SampleRate:
        type: integer
        format: int32
      SelfAware:
        description: Can calculate the confidence
        type: boolean
      Scaler:
        description: The maximum value of the abs(measurement value).
        type: number
        format: float

  ScalarFormat:
    type: integer
    format: uint8
    enum:
      - name: Int16
        value: 0
      - name: General
        value: 1
