# OVERVIEW

Clone this repo under movesense-device-lib/samples -folder.

Provider binary encoding manifest can be generated using the command below.
This is needed on the mobile application side to decode the WB structs.

## Prerequisited
* wbres tool in path (can be found in movesense-device-lib)
* mobile/app_root.yaml in sync with the app_root.yaml of the Movesense app

## Script to generate .wbr

```
> cd mobile
> ./get_wbr.sh
> cp recipe.wbr [android raw resource folder]
```
