#include "movesense.h"

#include "RecipeClient.h"
#include "common/core/debug.h"
#include "app-resources/resources.h"

#include "ui_ind/resources.h"


const size_t BLINK_PERIOD_MS = 2000;
const char* const RecipeClient::LAUNCHABLE_NAME = "RecipeClient";

WB_RES::RecipeItemId recipeItems[] = {
  WB_RES::RecipeItemIdValues::SCALAR,
  WB_RES::RecipeItemIdValues::BUFFER,
  WB_RES::RecipeItemIdValues::FEATURE,
  WB_RES::RecipeItemIdValues::TRIGGER,
  WB_RES::RecipeItemIdValues::TRIGGERWITHCLASSIFIERS,
  WB_RES::RecipeItemIdValues::CLUSTER,
  WB_RES::RecipeItemIdValues::LOGISTICREGRESSION,
  WB_RES::RecipeItemIdValues::DECISIONTREE
};

static const size_t RESOURCE_PATH_BUFFER_SIZE = 60;
static const size_t REMOTE_RESOURCE_TIMEOUT = 3000;

static const whiteboard::LocalResourceId sProviderResources[] = {
    WB_RES::LOCAL::ML_MANAGER::LID
};

static const whiteboard::ExecutionContextId sExecutionContextId =
    WB_RES::LOCAL::ML_MANAGER::EXECUTION_CONTEXT;

WB_RES::ItemIdWithIndexType params;

RecipeClient::RecipeClient()
    : ResourceClient(WBDEBUG_NAME(__FUNCTION__), sExecutionContextId),
      ResourceProvider(WBDEBUG_NAME(__FUNCTION__), sExecutionContextId),
      LaunchableModule(LAUNCHABLE_NAME, WB_EXEC_CTX_APPLICATION),
      mActiveRemoteWhiteboardId(0),
      mActiveRemoteWhiteboardName(""),
      mActiveRemoteGetRequestId(wb::ID_INVALID_REQUEST),
      mRecipeResourceId(wb::ID_INVALID_RESOURCE),
      mRecipeResourceRequest(wb::ID_INVALID_REQUEST)
{
    mTimer = whiteboard::ID_INVALID_TIMER;
}

RecipeClient::~RecipeClient()
{
}

bool RecipeClient::initModule()
{
    if (registerProviderResources(sProviderResources) != whiteboard::HTTP_CODE_OK)
    {
        return false;
    }
    mModuleState = WB_RES::ModuleStateValues::INITIALIZED;
    return true;
}

void RecipeClient::deinitModule()
{
    mModuleState = WB_RES::ModuleStateValues::UNINITIALIZED;
}

/** @see whiteboard::ILaunchableModule::startModule */
bool RecipeClient::startModule()
{
    mModuleState = WB_RES::ModuleStateValues::STARTED;

    // Start LED timer. true = trigger repeatedly
    mTimer = ResourceClient::startTimer(BLINK_PERIOD_MS, true);
    asyncSubscribeLocalResource(WB_RES::LOCAL::NET::LID);
    return true;
}

/** @see whiteboard::ILaunchableModule::startModule */
void RecipeClient::stopModule()
{
    // Stop LED timer
    ResourceClient::stopTimer(mTimer);
    mTimer == whiteboard::ID_INVALID_TIMER;
}

void RecipeClient::onTimer(whiteboard::TimerId timerId)
{
    if (timerId != mTimer)
    {
        return;
    }

    uint16_t indicationType = 2; // SHORT_VISUAL_INDICATION, defined in ui/ind.yaml

    // Make PUT request to trigger led blink
    // DEBUGLOG("Blink");
    asyncPut(WB_RES::LOCAL::UI_IND_VISUAL::ID, AsyncRequestOptions::Empty, indicationType);

    // Resolving recipe resource ID
    /*
    if(mActiveRemoteWhiteboardId != 0 && mRecipeResourceId == wb::ID_INVALID_RESOURCE){
      char remoteResourcePath[RESOURCE_PATH_BUFFER_SIZE];
      snprintf(remoteResourcePath,
               sizeof(remoteResourcePath),
               "/net/%s/Ml/Recipe/1",
               mActiveRemoteWhiteboardName);
      asyncGetResource(remoteResourcePath,
                AsyncRequestOptions(&mRecipeResourceRequest, REMOTE_RESOURCE_TIMEOUT));
    }
    */
    // Make GET to the /Ml/Recipe URL
    /*
    WB_RES::ItemIdWithIndexType params;
    params.itemId = WB_RES::RecipeItemIdValues::SCALAR;
    params.index = 1;
    if(mActiveRemoteWhiteboardName != "" && mRecipeResourceId != wb::ID_INVALID_RESOURCE){
      asyncGet(mRecipeResourceId, AsyncRequestOptions(&mActiveRemoteGetRequestId,0,false,true,false,true),params);
    }
    */
    /*
    if(mActiveRemoteWhiteboardName != "" && mRecipeResourceId != wb::ID_INVALID_RESOURCE){
      asyncGet(mRecipeResourceId, AsyncRequestOptions(&mActiveRemoteGetRequestId),"Yo 69");
    }
    */

}

void RecipeClient::onPostRequest(const whiteboard::Request& request,
                                      const whiteboard::ParameterList& parameters)
{
    if (mModuleState != WB_RES::ModuleStateValues::STARTED)
    {
        return returnResult(request, wb::HTTP_CODE_SERVICE_UNAVAILABLE);
    }
    WB_RES::EndpointHandle handle = WB_RES::LOCAL::ML_MANAGER::POST::ParameterListRef(parameters).getRecipeHandle();

    DEBUGLOG("onPostRequest(handle.id = %d)",handle.id);

//    if(mActiveRemoteWhiteboardId != 0 && mRecipeResourceId == wb::ID_INVALID_RESOURCE){
    if(mActiveRemoteWhiteboardId != 0){
      char remoteResourcePath[RESOURCE_PATH_BUFFER_SIZE];
      snprintf(remoteResourcePath,
               sizeof(remoteResourcePath), "/net/%s/Ml/Recipe/%d", mActiveRemoteWhiteboardName, handle.id);
      DEBUGLOG("asyncGetResource(%s)", remoteResourcePath);
      asyncGetResource(remoteResourcePath,
                AsyncRequestOptions(&mRecipeResourceRequest, REMOTE_RESOURCE_TIMEOUT));
    }

    WB_RES::EndpointHandle response;
    response.id = mNextClassifierIndex++;
    response.pathId = 0;

    DEBUGLOG("returnResult(): resultCode = %d", whiteboard::HTTP_CODE_CREATED);
    return returnResult(request, whiteboard::HTTP_CODE_CREATED, ResponseOptions::Empty, response);

}

void RecipeClient::onNotify(wb::ResourceId resourceId, const wb::Value& value, const wb::ParameterList& parameters)
{
  if (resourceId.localResourceId == WB_RES::LOCAL::NET::LID)
  {
      uint8 const notificationType = parameters[0].convertTo<uint8>();
      uint8 const whiteboardId = parameters[1].convertTo<uint8>();


      if (notificationType == WB_RES::RoutingTableNotificationTypeValues::ROUTE_NOTIFICATION_NEW)
      {
          DEBUGLOG("onNotify() - notificationType: ROUTE_NOTIFICATION_NEW");
          mActiveRemoteWhiteboardId = whiteboardId;
          strncpy_s(mActiveRemoteWhiteboardName, value.convertTo<char const*>(), sizeof(mActiveRemoteWhiteboardName));
          DEBUGLOG("NEW DEVICE DISCOVERED: WB NAME = %s", mActiveRemoteWhiteboardName);
      }
      else
      { // error or closed connection
          if (whiteboardId == mActiveRemoteWhiteboardId)
          {
              mRecipeResourceId = wb::ID_INVALID_RESOURCE;
              mActiveRemoteWhiteboardId = 0;
          }
      }
  }
}

void RecipeClient::onGetResourceResult(wb::RequestId requestId, wb::ResourceId resourceId, wb::Result result)
{
    DEBUGLOG("onGetResourceResult() - result = %d, requestId = %d", result,requestId);
    if (result == wb::HTTP_CODE_OK)
    {
      if (requestId == mRecipeResourceRequest)
      {
        mRecipeResourceId = resourceId;
        DEBUGLOG("mRecipeResourceId = %d", resourceId);

        // Make the first AsyncGet for the Recipe
        params.itemId = recipeItems[0];
        params.index = 0;
        if(mActiveRemoteWhiteboardName != "" && mRecipeResourceId != wb::ID_INVALID_RESOURCE){
          asyncGet(mRecipeResourceId, AsyncRequestOptions(&mActiveRemoteGetRequestId,0,false,true,false,true),params);
//          asyncGet(mRecipeResourceId, AsyncRequestOptions(&mActiveRemoteGetRequestId),params);
        }
      }
    }
}

void RecipeClient::onGetResult(wb::RequestId requestId,
                               wb::ResourceId resourceId,
                               wb::Result resultCode,
                              const wb::Value& resultData)
{
    DEBUGLOG("onGetResult(): resultCode = %d, resultData.getType() = %d", resultCode, resultData.getType());
    if (requestId == mActiveRemoteGetRequestId)
    {
      /*
        switch (resultData.getType())
        {
          case wb::WB_TYPE_NONE:
            break;
          case wb::WB_TYPE_UINT8:
            uint8_t data = resultData.convertTo<uint8_t>();
            DEBUGLOG("onGetResult(): resultCode = %d, data = %d", resultCode, data);
            break;
          case wb::WB_TYPE_STRUCTURE:
            DEBUGLOG("wb::WB_TYPE_STRUCTURE: DataTypeId = %u", resultCode, resultData.getReceiverDataTypeId());
            switch (resultData.getReceiverDataTypeId())
            {
                case WB_RES::ScalarParameters::DATA_TYPE_ID:
                  DEBUGLOG("- ScalarParameters: ...");
                  break;
                case WB_RES::ItemIdWithIndexType:
                  DEBUGLOG("- ItemIdWithIndexType: ...");
                  break;
            }
            // intentional fall through
          default:
              break;
        }
        */
        mActiveRemoteGetRequestId = wb::ID_INVALID_REQUEST;
        switch(resultCode){
          case 100: // Move to next id
            params = resultData.convertTo<WB_RES::ItemIdWithIndexType>();
            asyncGet(mRecipeResourceId, AsyncRequestOptions(&mActiveRemoteGetRequestId,0,false,true,false,true),params);
            break;
          case 200:
            params.index++;
            asyncGet(mRecipeResourceId, AsyncRequestOptions(&mActiveRemoteGetRequestId,0,false,true,false,true),params);
            break;
          case 404:
            break;
        }
    }
}
