#pragma once

#include <whiteboard/LaunchableModule.h>
#include <whiteboard/ResourceClient.h>
#include <whiteboard/ResourceProvider.h>

class RecipeClient FINAL : private whiteboard::ResourceClient,
                           private whiteboard::ResourceProvider,
                           public whiteboard::LaunchableModule

{
public:
    /** Name of this class. Used in StartupProvider list. */
    static const char* const LAUNCHABLE_NAME;
    RecipeClient();
    ~RecipeClient();

    /// @ see::ResourceClient::onNotify
    virtual void onNotify(wb::ResourceId resourceId, const wb::Value& value, const wb::ParameterList& parameters) OVERRIDE;

    ///  @see whiteboard::ResourceClient::onGetResourceResult
    virtual void onGetResourceResult(wb::RequestId requestId, wb::ResourceId resourceId, wb::Result resultCode) OVERRIDE;

    /**
    *	POST request handler.
    *
    *	@param requestId ID of the request
    *	@param clientId ID of the client that should receive the result
    *	@param resourceId ID of the associated resource
    *	@param parameters List of parameters for the request
    *	@return Result of the operation
    */
    virtual void
    onPostRequest(const whiteboard::Request& request, const whiteboard::ParameterList& parameters) OVERRIDE;

    /// @see whiteboard::ResourceClient::onGetResult
    // virtual void
    // onGetResult(wb::RequestId requestId, wb::ResourceId resourceId, wb::Result resultCode, const wb::Value& resultData) OVERRIDE;

    /// @see whiteboard::ResourceClient::onPostResult
    virtual void
    onGetResult(wb::RequestId requestId, wb::ResourceId resourceId, wb::Result resultCode, const wb::Value& resultData) OVERRIDE;

private:
    /** @see whiteboard::ILaunchableModule::initModule */
    virtual bool initModule() OVERRIDE;

    /** @see whiteboard::ILaunchableModule::deinitModule */
    virtual void deinitModule() OVERRIDE;

    /** @see whiteboard::ILaunchableModule::startModule */
    virtual bool startModule() OVERRIDE;

    /** @see whiteboard::ILaunchableModule::stopModule */
    virtual void stopModule() OVERRIDE;

protected:
    /**
    *	Timer callback.
    *
    *	@param timerId Id of timer that triggered
    */
    virtual void onTimer(whiteboard::TimerId timerId) OVERRIDE;

private:
    whiteboard::TimerId mTimer;
    uint8_t mActiveRemoteWhiteboardId;
    uint8_t mNextClassifierIndex = 1;
    char mActiveRemoteWhiteboardName[20];
    wb::ResourceId mRecipeResourceId;
    wb::RequestId mRecipeResourceRequest;
    wb::RequestId mActiveRemoteGetRequestId;
};
